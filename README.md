# Python Zabbix sender

Minimal Python Zabbix sender library implementation that works with IPv6.
It allows you to send metrics from your Python code to given Zabbix server.

This is not standalone application.
There is already
[zabbix_sender](https://www.zabbix.com/documentation/current/en/manual/concepts/sender)
implemented.


## Usage

```Python
from zabbix_sender.sender import ZabbixMetrics, send_metrics

metrics = [ZabbixMetrics("host-to-monitor", "key", "value"),]
response = send_metrics(m).check()
if response:
	print("success")
else:
	print("failed")
```


## Dependencies

Build system, dependency management, packaging:
- [Poetry](https://python-poetry.org/)

Dev tools:
- [black](https://github.com/psf/black)
- [isort](https://pycqa.github.io/isort/index.html)
- [Flake8-pyproject](https://github.com/john-hen/Flake8-pyproject)
- [mypy](http://www.mypy-lang.org/)

Tests:
- [pytest](https://docs.pytest.org/)
- [pytest-cov](https://github.com/pytest-dev/pytest-cov)


## Project setup

Install `poetry`:
```
curl --silent --show-error --location https://install.python-poetry.org | python3 -
```

Create virtual environment by tool of your choice and activate it e.g.:
```
poetry shell
```

Install the project and its dependencies in a virtual environment:
```
poetry install
```

## Dev tools usage

Formating:
```
black .
```

Sorting imports:
```
isort .
```

Linting:
```
flake8p .
```

Type check:
```
mypy .
```


## Tests and coverage usage

To run tests:
```
pytest -v tests/unit
```

To generate coverage report:
```
pytest --cov=zabbix_sender/ tests/unit
```

There are also integration tests implemented see dedicated
[README](./tests/integration/README.md).
