# Integration tests

This folder contains integration tests powered by `pytest`.
Their purpose is to test our sender with real Zabbix server counterpart to
be sure that the Zabbix communication protocol is implemented correctly and
the sender works in real life.

For running this test one needs running Zabbix server, a database system and web
server with Zabbix web frontend. This can be done quite conviniently using
[Docker](https://www.docker.com/) and dedicated Docker images/containers.

To setup all the above mentioned follow these steps:

1. Install Docker.

2. Create Docker network:
```
docker network create \
	--subnet 172.20.0.0/16 \
	--ip-range 172.20.240.0/20 \
	zabbix-net
```

3. Run empty PostgreSQL server instance:
```
sudo docker run \
	--name postgres-server \
	--network=zabbix-net \
	--env POSTGRES_USER="zabbix" \
	--env POSTGRES_PASSWORD="zabbix_pwd" \
	--env POSTGRES_DB="zabbix" \
	--detach \
	postgres:latest
```

4. Run Zabbix server instance and link the instance with created PostgreSQL
server instance:
```
sudo docker run \
	--name zabbix-server-pgsql \
	--network=zabbix-net \
	--env DB_SERVER_HOST="172.20.240.1" \
	--env POSTGRES_USER="zabbix" \
	--env POSTGRES_PASSWORD="zabbix_pwd" \
	--env POSTGRES_DB="zabbix" \
	--publish 10051:10051 \
	--detach \
	zabbix/zabbix-server-pgsql:alpine-5.4-latest
```

5. Start Zabbix web interface and link the instance with created PostgreSQL
server and Zabbix server instances:
```
sudo docker run \
	--name zabbix-web-nginx-pgsql \
	--network=zabbix-net \
	--env ZBX_SERVER_HOST="172.20.240.2" \
	--env DB_SERVER_HOST="172.20.240.1" \
	--env POSTGRES_USER="zabbix" \
	--env POSTGRES_PASSWORD="zabbix_pwd" \
	--env POSTGRES_DB="zabbix" \
	--publish 80:8080 \
	--detach \
	zabbix/zabbix-web-nginx-pgsql:alpine-5.4-latest
```

6. Access web interface on [localhost](http://localhost).

	Default credentials:
	- username: Admin
	- password: zabbix

7. [Setup a host](https://www.zabbix.com/documentation/current/en/manual/config/hosts/host)
to monitor.

- name: test-host

8. [Setup Trapper](https://www.zabbix.com/documentation/current/en/manual/config/items/itemtypes/trapper)
items. The following is recomended setup:

- name: numeric-unsigned-item
- key: numeric-unsigned-key
- type of information: numeric (unsigned)
---
- name: numeric-float-item
- key: numeric-float-key
- typ of information: numeric (float)
---
- name: character-item
- key: character-key
- typ of information: character
---
- name: text-item
- key: text-key
- typ of information: text
---
- name: log-item
- key: log-key
- typ of information: log


9. Tests first with official
[zabbix_sender](https://www.zabbix.com/documentation/current/en/manual/concepts/sender)
CLI utility:
```
zabbix_sender \
	--zabbix-server localhost \
	--port 10051 \
	--host "test-host" \
	--key "numeric-unsigned-key" \
	--value "123"
```

You should see in Monitoring tab of `test-host` that `numeric-unsigned-key` has
a new value. If not something is wrong.

10. Run integration tests.
```
pytest -v tests/integration
```
