import pytest

from zabbix_sender.sender import ZabbixMetrics, send_metrics

SERVER_HOST = "localhost"
SERVER_PORT = 10051


@pytest.mark.parametrize(
    "metrics",
    [
        [
            ZabbixMetrics("test-host", "numeric-unsigned-key", 123456),
        ],
        [
            ZabbixMetrics("test-host", "numeric-float-key", 1e-5),
        ],
        [
            ZabbixMetrics("test-host", "character-key", "aaaaa"),
        ],
        [
            ZabbixMetrics(
                "test-host",
                "text-key",
                "aaaaadsfuaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
                "aaaaaaaalashlkfdhkashfdkhaskdfhkahsdkfhksahdfhsafkdjfsakjhfkdj"
                "kasdjfsfajhdkjfhkjsakfjdkjasfkdjhaskjdkjhfksajhdkfhksahdfkhska"
                "dhfkhsadkfhkahskjfhksjahdkfjhksajdhfkjhksahdfkhsakdjfhkjsahdkf"
                "jhksjdhkfjhksahfdkjshakdjhfkjhasdkjkjsahdfkhsdahfk",
            ),
        ],
        [
            ZabbixMetrics(
                "test-host",
                "log-key",
                "sdifosiajdfoijshdfkjhsdfll;ldkjfal;dsjfljsad;lfdjskkkkkkkkkkkk"
                "kkkkkkkkkkkkkkkkkkkkkkkkkdjncccccccccccccccccccccccca;ssssssss"
                "sssssssssssssssssssssssssssssssssssssssssssssssssjffffffffffff"
                "fffffffffffffdlkdlllllllllllllllllllllllllllllllllllllllllsjjj"
                "jjjjjjjjjjjjjjjjjjjjljcmmmmmmmmmmmmmm;aaaaaaaaaaaaaaaaaaaaaaaa"
                "aaaajffffffffffffsgha",
            ),
        ],
    ],
)
def test_send(metrics):
    resp = send_metrics(metrics=metrics, server_host=SERVER_HOST, server_port=SERVER_PORT)
    assert resp.check()
