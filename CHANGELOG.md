# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2022-12-22

### Changed

- Use pytest for tests
- Project structure - merge with template project
- Use poetry instead of setuptools
- Separate integration tests from unit tests

### Removed

- ZabbixSender class

### Added

- Gitlab CI
- Dev script
- Integration tests setup manual

## [0.1.0] - 2022-12-13

### Added

- Initial implementation
