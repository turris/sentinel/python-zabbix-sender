#!/usr/bin/env python3
# flake8: noqa
# type: ignore

from zabbix_sender.sender import *

metrics = [
    ZabbixMetrics("test-host", "numeric-unsigned-key", 66666),
]
sock, sockaddr = create_sock("localhost", 10051)
messages = create_messages(metrics)
print(messages)
request = create_request(messages)
print(request)
packet = create_packet(request)
print(packet)
sock.connect(sockaddr)
sock.sendall(packet)
response_json = get_response(sock)
print(response_json)
sock.close()

resp = ZabbixResponse(*parse_zabbix_resp(response_json))
print(resp)
