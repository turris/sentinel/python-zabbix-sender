import json
import socket
import struct


class ZabbixMetrics:
    def __init__(self, host, key, value, clock=None, ns=None):
        self.host = host
        self.key = key
        self.value = value
        if clock:
            self.clock = clock
        if ns:
            self.ns = ns

    def __repr__(self):
        result = json.dumps(self.__dict__, ensure_ascii=False)
        return result


class ZabbixResponse:
    def __init__(self, response, processed, failed, total, seconds_spent):
        self.response = response
        self.processed = processed
        self.failed = failed
        self.total = total
        self.seconds_spent = seconds_spent

    def __repr__(self):
        return (
            f"Zabbix response:\n"
            f"\tresponse: {self.response}\n"
            f"\tprocessed: {self.processed}\n"
            f"\tfailed: {self.failed}\n"
            f"\ttotal: {self.total}\n"
            f"\tseconds_spent: {self.seconds_spent}"
        )

    def check(self):
        """Checks whether all the metrics were successfully submitted.
        Returns True if yes otherwise returns False."""
        if self.response == "success" and self.processed == self.total:
            return True
        return False


def create_sock(host, port):
    sock = None
    for family, type, proto, canonname, sockaddr in socket.getaddrinfo(
        host=host, port=port, family=socket.AF_UNSPEC, type=socket.SOCK_STREAM
    ):
        try:
            sock = socket.socket(family=family, type=type, proto=proto)
        except OSError:
            sock = None
            continue
        break
    if sock is None:
        raise Exception("Error")
    return sock, sockaddr


def create_messages(metrics):
    messages = []
    for m in metrics:
        messages.append(str(m))
    return messages


def create_request(messages):
    msg = ",".join(messages)
    request = '{{"request":"sender data","data":[{msg}]}}'.format(msg=msg)
    request = request.encode("utf-8")
    return request


def create_packet(request):
    data_len = struct.pack("<Q", len(request))
    packet = b"ZBXD\x01" + data_len + request
    return packet


def recv(sock, count):
    buf = b""
    while len(buf) < count:
        chunk = sock.recv(count - len(buf))
        if not chunk:
            break
        buf += chunk
    return buf


def get_response(sock):
    response_header = recv(sock, 13)
    if not response_header.startswith(b"ZBXD\x01") or len(response_header) != 13:
        result = None
    else:
        response_len = struct.unpack("<Q", response_header[5:])
        response_body = recv(sock, response_len[0])
        result = response_body
    return result


def parse_zabbix_resp(resp):
    attributes = json.loads(resp)
    mydict = dict(
        (k.strip(), v.strip())
        for k, v in (item.split(":") for item in attributes["info"].split(";"))
    )
    return (
        attributes["response"],
        mydict["processed"],
        mydict["failed"],
        mydict["total"],
        mydict["seconds spent"],
    )


def send_metrics(metrics, server_host="127.0.0.1", server_port=10051):
    sock, sockaddr = create_sock(server_host, server_port)
    messages = create_messages(metrics)
    request = create_request(messages)
    packet = create_packet(request)

    sock.connect(sockaddr)
    sock.sendall(packet)
    response_json = get_response(sock)
    sock.close()

    return ZabbixResponse(*parse_zabbix_resp(response_json))
